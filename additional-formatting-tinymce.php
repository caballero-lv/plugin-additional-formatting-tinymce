<?php
/*
Plugin Name: Additional formatting for tinymce
Version: 1.0.0
Author: Caballero.lv
Author URI: https://www.caballero.lv/
Text Domain: additionaformattingtinymce

Copyright 2013-2016 SearchWP, LLC

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, see <http://www.gnu.org/licenses/>.
*/

// exit if accessed directly
if (!defined('ABSPATH')) {
    exit;
}

define('SEARCHWP_VERSION', '1.0.0');
define('SEARCHWP_PREFIX', 'adfoti_');
define('SEARCHWP_DBPREFIX', 'adfoti_');


// minimum WordPress version requirement
$wp_version = get_bloginfo('version');
if (version_compare($wp_version, '3.5', '<')) {
    /** @noinspection PhpIncludeInspection */
    require_once ABSPATH . '/wp-admin/includes/plugin.php';
    deactivate_plugins(__FILE__);
    wp_die(esc_html(__('Additional formatting for tinymce requires WordPress 3.5 or higher. Please upgrade before activating this plugin.')));
}

/**
 * Class AdditionalFormattingTinymce
 */
class AdditionalFormattingTinymce
{

    /**
     * @var AdditionalFormattingTinymce singleton
     */
    private static $instance;

    /**
     * @var string Plugin name
     */
    public $pluginName = 'AdditionalFormattingTinymce';

    /**
     * @var string Plugin textdomain, used in localization
     */
    public $textDomain = 'additionaformattingtinymce';

    /**
     * @var string The config directory
     */
    public $dir;

    /**
     * @var array Stores all AdditionalFormattingTinymce settings
     */
    public $settings;

    /**
     * Singleton
     * @return AdditionalFormattingTinymce
     */
    public static function instance()
    {
        if (!isset(self::$instance) && !(self::$instance instanceof AdditionalFormattingTinymce)) {

            self::$instance = new AdditionalFormattingTinymce;
            self::$instance->init();

        }

        return self::$instance;
    }

    /**
     * Initialization routine. Sets version, directory, url, adds WordPress hooks, includes includes, triggers index
     */
    function init()
    {
        if ($uploadDir = wp_upload_dir()) {
            $this->dir = $uploadDir['basedir'] . '/aftinymce';
        } else {
            $this->dir = dirname(__FILE__) . '/assets';
        }

        //Copy configuration data for alteration
        if (!file_exists($this->dir)) {
            wp_mkdir_p($this->dir);
        }
        if (!file_exists($this->dir . '/config.json')) {
            if (file_exists(dirname(__FILE__) . '/assets/config.json') && $this->dir != dirname(__FILE__) . '/assets') {
                copy(dirname(__FILE__) . '/assets/config.json', $this->dir . '/config.json');
            }
        }
        if (!file_exists($this->dir . '/aftinymce.css')) {
            if (file_exists(dirname(__FILE__) . '/assets/aftinymce.css') && $this->dir != dirname(__FILE__) . '/assets') {
                copy(dirname(__FILE__) . '/assets/aftinymce.css', $this->dir . '/aftinymce.css');
            }
        }

        //Read configuration data
        if (file_exists($this->dir . '/config.json')) {
            $tmp = file_get_contents($this->dir . '/config.json');
            $this->settings = json_decode($tmp, true);
        }

        // hooks
        add_action( 'mce_css', array( $this, 'add_editor_styles' ) );
        add_filter('block_local_requests', '__return_false');
        add_filter('tiny_mce_before_init', array($this, 'insert_formats'), 9999);

    }

    function add_editor_styles( $mce_css ){

        if ( ! empty( $mce_css ) ){
            $mce_css .= ',';
        }

        $mce_css .= WP_CONTENT_URL . '/uploads/aftinymce/aftinymce.css';

        return $mce_css;
    }

    /**
     * @param $init_array
     * @return mixed
     */
    function insert_formats($init_array)
    {

        if(isset($this->settings['formats'])) {
            // Define the style_formats array
            $style_formats = $this->settings['formats'];

            // Insert the array, JSON ENCODED, into 'style_formats'
            $init_array['style_formats'] = json_encode($style_formats);
        }

        return $init_array;

    }
}

if (!function_exists('aft_init')) {

    /**
     * @return AdditionalFormattingTinymce
     */
    function aft_init()
    {
        global $aftinymce;

        if (is_admin() || apply_filters('aft_init', true)) {
            $aftinymce = AdditionalFormattingTinymce::instance();
        }

        return $aftinymce;
    }
}

// @codingStandardsIgnoreStart
if (!function_exists('AFT')) {

    /**
     * @return AdditionalFormattingTinymce
     */
    function AFT()
    {
        return AdditionalFormattingTinymce::instance();
    }

    // initialize AdditionalFormattingTinymce Singleton
    aft_init();

}
// @codingStandardsIgnoreEnd